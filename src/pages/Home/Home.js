import FooterBlock from '@/assets/footer/footer.vue';
import Navbar      from '@/assets/navbar/navbar.vue';

import { Swiper, SwiperSlide }  from 'swiper/vue';
import SwiperCore, { 
    Pagination,
    Mousewheel,
    Autoplay,
    Controller } from 'swiper';

// mixins
import resizeMixin from "@/assets/mixins/windowResize";
import mainMixin   from "@/assets/mixins/main";

//  swiper styles
import 'swiper/swiper.scss';
import 'swiper/components/pagination/pagination.scss';

SwiperCore.use([Pagination, Mousewheel, Autoplay, Controller]);

export default {
    name: "Home",

    mixins: [resizeMixin, mainMixin],

    components: {
        FooterBlock,
        Navbar,
        Swiper,
        SwiperSlide
    },

    data() {
        return {
            swiperPage: 0,
            serverPage: 0,

            mainSwiper: null,
            serverSwiper: null,
            navMobile: false,

            pages: ['casa', 'sobre el proyecto', 'cómo jugar'],
            cards: [
                {
                    title: 'COMPRAR LICENCIA Gta V',
                    description: 'Si el juego ya se ha comprado, ve al siguiente paso.',
                    button: 'Comprar'
                },

                {
                    title: 'INSTALAR EN PC Y EMPEZAR RAGE MP',
                    description: '',
                    button: 'descargar'
                },

                {
                    title: 'Encuenta VESPUCCI',
                    description: 'Para encontrar nuestros servidores, debe abrir el modo multijugador, hacer clic en el botón "Servidores" y luego buscar por el nombre "Vespucci".',
                    button: 'Conectar'
                },
            ]
        }
    },

    methods: {
        slideTo(i) {
            this.mainSwiper.slideTo(i);
            this.navMobile = false;
        },

        mainSwiperChange(swiper) {
            this.swiperPage = swiper.activeIndex;
            this.navMobile = false;
        },

        serverSwiperChange(swiper) {
            this.serverPage = swiper.activeIndex;
        },

        mainSwiperInit(swiper) {
            this.mainSwiper = swiper;

            const route = this.$route;

            if (route.query.startsWith != null) {
                this.mainSwiper.slideTo(route.query.startsWith);
                this.$router.replace({ query: {} })
            }
        },

        serverSwiperInit(swiper) {
            this.serverSwiper = swiper;
        }
    }
}