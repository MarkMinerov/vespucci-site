import FooterBlock     from '@/assets/footer/footer.vue';
import Navbar          from '@/assets/navbar/navbar.vue';
import { Form, Field } from 'vee-validate';

// mixins
import mainMixin   from "@/assets/mixins/main";
import resizeMixin from "@/assets/mixins/windowResize";

export default {
    name: 'Donate',

    mixins: [mainMixin, resizeMixin],

    components: {
        FooterBlock,
        Navbar,
        Form,
        Field
    },

    data() {
        return {
            pages: ['casa', 'sobre el proyecto', 'cómo jugar'],
            nickname: null,
            sum: null,
            email: null,
            vcPerEuro: 20,
            navMobile: false,
            showTip: false,
            mobileHint: false
        }
    },

    methods: {
        submitForm(values) {
            const email    = values.email,
                  nickname = values.nickname,
                  sum      = values.sum;

            if (!email || !nickname || !sum) {
                return false;
            }

            if (!/[a-z]+. [a-z]+./i.test(nickname) ||
                !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(email) ||
                isNaN(parseInt(sum))) {
                return false;
            }

            console.log('fine!');
        },

        openPage(index) {
            this.$router.push({ name: 'Home', query: { startsWith: index } });
        },

        switchHint() {
            this.mobileHint = !this.mobileHint;

            const block = this.$refs.donate;
            this.$refs.donate.scrollTop = 2000;

            console.log()
        }
    },

    computed: {
        willGet() {
            return isNaN(this.sum * this.vcPerEuro) ? 0 : this.sum * this.vcPerEuro
        }
    }
}