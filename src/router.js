import { createRouter, createWebHistory } from 'vue-router'

const Home   = () => import('./pages/Home/Home.vue');
const Donate = () => import('./pages/Donate/Donate.vue');

const routes = [
    { path: '/',       name: 'Home',   component: Home   },
    { path: '/donate', name: 'Donate', component: Donate },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router;