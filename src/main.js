import { createApp } from 'vue'
import App from './App.vue'
import router from './router.js'
import store from './store'
import VueKinesis from "vue-kinesis";

import './assets/global.scss';

const app = createApp(App)
.use(store)
.use(router);

app.use(VueKinesis);

app.mount('#app');