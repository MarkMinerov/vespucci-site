import resizeMixin from '../mixins/windowResize';

export default {
    name: 'navbar',
    mixins: [resizeMixin],
    emits: ['open-page', 'update:navMobile'],
    props: {
        active: Number,
        navMobile: Boolean
    },
    
    data() {
        return {
            pages: ['casa', 'sobre el proyecto', 'cómo jugar']
        }
    },

    methods: {
        switchNav() {
            this.$emit('update:navMobile', !this.navMobile)
        },

        actionMobile(i) {
            this.$emit('open-page', i);
            this.$emit('update:navMobile', false)
        }
    }
}