export default {
    data() {
        return {
            footer: false,
            debounce: null,
            touchStartY: null
        }
    },

    methods: {
        showFooter(event) {            
            if (this.debounce != null) return event.stopPropagation();

            if (event.deltaY > 0) {
                this.setDebounce();

                if (!this.footer) {
                    this.footer = true;
                    return event.stopPropagation();
                }

                return;
            }
            
            if (event.deltaY < 0) {
                this.setDebounce();

                if (this.footer) {
                    this.footer = false;
                    return event.stopPropagation();
                }

                return;
            }
        },

        preventForNumber(event) {
            if ((event.target.value + event.key).search( /[^[0-9]]*/gm ) > -1) {
                event.preventDefault();
                return false;
            } else return true;
        },

        setDebounce() {
            this.debounce = setTimeout(() => {
                clearTimeout(this.debounce);
                this.debounce = null;
            }, 1000);
        },

        touchStart(event) {
            const touch = event.touches[0] || event.changedTouches[0];
            this.touchStartY = touch.pageY;
        },

        touchEnd(event) {
            const touch = event.touches[0] || event.changedTouches[0],
                  difference = touch.pageY - this.touchStartY;

            // slide up
            if (difference < -200) {
                this.footer = true;
                event.stopPropagation();
            } 
            
            // slide down
            else if (difference > 0) {
                this.footer = false;
            }

            this.touchStartY = null;
        },

        isMovable(strength) {
            return !this.isMobile ? strength : 0;
        }
    }
}