export default {
    name: 'footer-block',

    data() {
        return {
            socials: [
                {
                    path: 'youtube',
                    text: 'canal de Youtube'
                },

                {
                    path: 'instagram',
                    text: 'página de instagram'
                },

                {
                    path: 'twitter',
                    text: 'página de twitter'
                },

                {
                    path: 'discord',
                    text: 'canal de DISCORD'
                },
            ]
        }
    }
}